//
//  HPDemoTests.swift
//  HPDemoTests
//
//  Created by harry on 6/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import HPDemo

class HPDemoTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLoadJobsJSON() {
        // Given
        //   Nothing
        // When
        let jobsJSONExpectation = XCTestExpectation(description: "jobs JSON loaded")
        JobLoader.load(completionHandler: { (data, error) in
            
            // Then
            XCTAssertNotNil(data)
            let json = try! JSON.init(data: data!)
            XCTAssertNotNil(json)
            XCTAssertNotNil(json["jobs"])
            XCTAssertEqual(json["jobs"].array!.count, 4)
            XCTAssertEqual(json["jobs"].array![0]["jobId"].int!, 423421)

            jobsJSONExpectation.fulfill()
        })
        
        wait(for: [jobsJSONExpectation], timeout: 5)
    }

}
