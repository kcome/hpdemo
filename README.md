
### Structure
- Since it's a simple one screen demo app, the ViewController with multiple different components are used instead of a complete suite of MVVM.
-- The current project could be refactored as MVVM
- ViewController is split into three components:
-- **UIKit** : Handles UIViewController and UIView related events
-- **Jobs** : Handles getting and processing data
-- **DataSource** : Populates data under UITableViewDataSource protocol
- **Job** and **Business** Entity are created within CoreData
-- Job has a one to many relationship which indicates one job could contains more Business

### Improvements
- Error checks/recovery in loading data from network
- Handle more than 8 **connectedBusinesses** in UICollectionView, with AutoLayout
- Complete UnitTest and UITest
- Close "CloseMenu" whenever a touch event happen (prevent multiple "CloseMenu" on screen at the same time)
- Handle the caching of connectedBusinesses thumbnail

