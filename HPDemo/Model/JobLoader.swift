//
//  JobLoader.swift
//  HPDemo
//
//  Created by harry on 5/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//

import Foundation
import Alamofire

struct JobLoader {

    public static func load(completionHandler: @escaping (Data?, Error?) -> Void) {
        request("https://s3-ap-southeast-2.amazonaws.com/hipgrp-assets/tech-test/jobs.json").responseData { response in
            switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                case let .failure(error):
                    completionHandler(nil, error)
            }
        }
    }

}
