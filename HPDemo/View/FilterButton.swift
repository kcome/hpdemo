//
//  FilterButton.swift
//  HPDemo
//
//  Created by harry on 5/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//

import UIKit

class FilterButton: UIButton {
    @IBInspectable
    var borderColor: UIColor = UIColor(red: 254.0/255.0, green: 112.0/255.0, blue: 39.0/255.0, alpha: 1.0)
    
    var borderLayer = CALayer()
    
    override var isSelected: Bool{
        didSet {
            self.layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.isSelected {
            addBorder(color: borderColor, width: 2.0)
        } else {
            self.borderLayer.removeFromSuperlayer()
        }
        setNeedsDisplay()
    }
    
    private func addBorder(color: UIColor, width: CGFloat) {
        borderLayer.backgroundColor = color.cgColor
        borderLayer.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(borderLayer)
    }
}
