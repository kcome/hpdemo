//
//  UIView+ParentViewController.swift
//  HPDemo
//
//  Created by harry on 6/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//

import UIKit

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as? UIViewController
            }
        }
        return nil
    }
}
