//
//  JobsTableViewCell.swift
//  HPDemo
//
//  Created by harry on 5/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//

import SwiftyJSON
import UIKit

fileprivate let kReusableCollectionCell = "reusableCollectionCell"
fileprivate let kOneRowCollectionViewHeight: CGFloat = 80
fileprivate let kTwoRowsCollectionViewHeight: CGFloat = 150

class JobsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var blockbox: UIView!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var postDate: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var hiredBizText: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var hiredBizCollection: UICollectionView!
    @IBOutlet weak var hiredBizHeight: NSLayoutConstraint!
    
    public var jobDetails: String?
    public var jobId: Int64 = 0
    
    public var hiredBiz: [Business]? = nil {
        didSet {
            // maximum 2 rows
            // TODO: handle more than 8 cells
            self.hiredBizHeight.constant = kTwoRowsCollectionViewHeight
            if let hiredBiz = self.hiredBiz {
                if hiredBiz.count == 0 {
                    self.hiredBizHeight.constant = 0
                }
                    // FIXME: Use '< 4' for demo two rows situation only. should be "<= 4"
                else if hiredBiz.count < 4 {
                    self.hiredBizHeight.constant = kOneRowCollectionViewHeight
                }
            } else {
                self.hiredBizHeight.constant = 0
            }
            hiredBizCollection.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: "HiredBusinessCell", bundle: Bundle.main)
        hiredBizCollection.register(nib, forCellWithReuseIdentifier: kReusableCollectionCell)
        self.customizeBorder(self.blockbox, color: UIColor.lightGray)
        self.customizeBorder(self.closeButton, color: UIColor.lightGray)
    }
    
    @IBAction func menuPressed(_ button: UIButton) {
        self.closeButton.isHidden = !self.closeButton.isHidden
    }
    
    @IBAction func closePressed(_ button: UIButton) {
        self.closeButton.isHidden = true
        if let parent = self.parentViewController as? ViewController {
            parent.closeJob(self.jobId)
        }
    }
    
    @IBAction func detailsPressed(_ button: UIButton) {
        if let details = self.jobDetails {
            let alert = UIAlertController(title: "View Details", message: "Job Details: \(details)", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.parentViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    // TODO: border customisation could be extension of UIView
    func customizeBorder(_ b: UIView, color: UIColor) {
        b.layer.borderWidth = 0.8
        b.layer.cornerRadius = 3.0
        b.layer.borderColor = color.cgColor
    }
}

extension JobsTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let hired = self.hiredBiz else {
            return 0
        }
        return hired.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = self.hiredBizCollection.dequeueReusableCell(withReuseIdentifier: kReusableCollectionCell, for: indexPath) as? HiredBuisnessCell else {
                fatalError()
        }
        if let hiredBiz = self.hiredBiz,
            indexPath.row < hiredBiz.count {
            cell.hired = hiredBiz[indexPath.row].isHired
            if let hiredThumb = hiredBiz[indexPath.row].thumbnail,
                let url = URL(string: hiredThumb) {
                cell.icon.load(url: url)
            }
        }
        return cell
    }
}
