//
//  HiredBusiness.swift
//  HPDemo
//
//  Created by harry on 5/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//

import UIKit

class HiredBuisnessCell: UICollectionViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var hiredLabel: UILabel!
    var hired: Bool = false {
        didSet {
            self.hiredLabel.layer.cornerRadius = 10.0
            self.hiredLabel.layer.masksToBounds = true
            self.hiredLabel.isHidden = !self.hired
        }
    }
}
