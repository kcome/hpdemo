//
//  ViewController+Jobs.swift
//  HPDemo
//
//  Created by harry on 6/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData


///
/// All job related operations are collected in this extension
///
extension ViewController {

    func initJobs() throws {
        self.allJobs = try context.fetch(Job.fetchRequest())
        self.filterJobs()
    }
    
    func loadJobs() {
        self.jobsTableView.refreshControl?.beginRefreshing()
        JobLoader.load(completionHandler: onReceiveJobs(_:Error:))
    }
    
    func onReceiveJobs(_ Data: Data?, Error: Error?) {
        self.jobsTableView.refreshControl?.endRefreshing()
        guard let data = Data else {
            print("error getting Jobs data: \(String(describing: Error))")
            return
        }
        guard let json = try? JSON(data: data) else {
            print("can't parse received data: \(data)")
            return
        }
        self.parseJobs(json["jobs"])
        self.filterJobs()
        self.jobsTableView.reloadData()
    }
    
    func parseJobs(_ jobs: JSON?) {
        guard let jobs = jobs?.array else {
            return
        }
        print("jobs json: \(jobs)")
        self.resetAllRecords("Job")
        self.resetAllRecords("Business")
        self.allJobs.removeAll()
        for job in jobs {
            guard let jobId = job["jobId"].int64 else {continue}
            let newJob = Job(entity: Job.entity(), insertInto: context)
            newJob.jobId = jobId
            newJob.category = job["category"].string
            newJob.postedDate = job["postedDate"].string
            newJob.status = job["status"].string
            newJob.detailsLink = job["detailsLink"].string
            if let connectedBiz = job["connectedBusinesses"].array {
                for business in connectedBiz {
                    let newBiz = Business(entity: Business.entity(), insertInto: context)
                    newBiz.thumbnail = business["thumbnail"].string
                    newBiz.businessId = business["businessId"].int64 ?? 0
                    newBiz.isHired = business["isHired"].bool ?? false
                    newBiz.parent = newJob
                    newJob.addToConnectedBusinesses(newBiz)
                }
            }
            self.allJobs.append(newJob)
        }
    }
    
    func resetAllRecords(_ entity: String) {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            let error = error as NSError
            fatalError("Deleting entity '\(entity)' failed: \(error)")
        }
    }
    
    func filterJobs() {
        self.filteredJobs.removeAll()
        for job in self.allJobs {
            if (self.closedJobs.isSelected) == (job.status == kStatusClosed) {
                self.filteredJobs.append(job)
            }
        }
    }
    
    public func closeJob(_ jobId: Int64) {
        if self.closedJobs.isSelected {
            return
        }
        for job in allJobs {
            if job.jobId == jobId {
                job.status = kStatusClosed
                break
            }
        }
        appDelegate.saveContext()
        self.filterJobs()
        self.jobsTableView.reloadData()
    }
}
