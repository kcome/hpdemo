//
//  ViewController+DataSource.swift
//  HPDemo
//
//  Created by harry on 6/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//

import UIKit

///
/// Provide UITableViewDataSource, from filteredJobs
///
extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredJobs.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: kReuseIdentifier, for: indexPath) as? JobsTableViewCell else {
            fatalError()
        }
        
        // Configure the cell...
        let job = filteredJobs[indexPath.row]
        cell.category.text = job.category
        if let postDate = job.postedDate {
            cell.postDate.text = "Posted: \(postDate)"
        }
        cell.status.text = job.status
        cell.jobDetails = job.detailsLink
        cell.jobId = job.jobId
        if let connectedBiz = job.connectedBusinesses,
            connectedBiz.count > 0 {
            cell.hiredBiz = job.connectedBusinesses?.allObjects as! [Business]?
            cell.hiredBizText.text = "You have hired \(connectedBiz.count) business\(connectedBiz.count > 1 ? "es" : "")"
        } else {
            cell.hiredBiz = nil
            cell.hiredBizText.text = "Connecting you with businesses"
        }
        return cell
    }
    
}
