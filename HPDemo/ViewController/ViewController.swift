//
//  ViewController.swift
//  HPDemo
//
//  Created by harry on 5/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//

import UIKit
import CoreData

internal let kStatusClosed = "Closed"
internal let kReuseIdentifier = "reuseIdentifier"

///
/// Only UIKit, UIViewController handling in this source
///
class ViewController: UIViewController {

    @IBOutlet weak var openJobs: UIButton!
    @IBOutlet weak var closedJobs: UIButton!
    @IBOutlet weak var jobsTableView: UITableView!
    
    internal var allJobs = [Job]()
    internal var filteredJobs = [Job]()

    internal let appDelegate = UIApplication.shared.delegate as! AppDelegate
    internal let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.closedJobs.isSelected = false
        self.openJobs.isSelected = true
        let nib = UINib(nibName: "JobsTableViewCell", bundle: Bundle.main)
        self.jobsTableView.register(nib, forCellReuseIdentifier:kReuseIdentifier)
        self.jobsTableView.rowHeight = UITableView.automaticDimension
        self.jobsTableView.refreshControl = UIRefreshControl()
        self.jobsTableView.refreshControl?.addTarget(self, action: #selector(refreshControlValueChanged), for: .valueChanged)
        self.loadJobs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        do {
            try self.initJobs()
            self.jobsTableView.reloadData()
        } catch let error as NSError {
            print("Fetch CoreData jobs fail: \(error), \(error.userInfo)")
        }
    }
    
    @IBAction func switchFilter(_ sender: Any?) {
        if let _ = sender as? UIButton {
            self.closedJobs.isSelected = !self.closedJobs.isSelected
            self.openJobs.isSelected = !self.openJobs.isSelected
            self.filterJobs()
            self.jobsTableView.reloadData()
        }
    }

    @objc func refreshControlValueChanged() {
        guard let refreshControl = self.jobsTableView.refreshControl, refreshControl.isRefreshing else { return }
        self.loadJobs()
    }
}
