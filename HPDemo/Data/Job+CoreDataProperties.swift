//
//  Job+CoreDataProperties.swift
//  HPDemo
//
//  Created by harry on 6/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//
//

import Foundation
import CoreData


extension Job {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Job> {
        return NSFetchRequest<Job>(entityName: "Job")
    }

    @NSManaged public var postedDate: String?
    @NSManaged public var jobId: Int64
    @NSManaged public var category: String?
    @NSManaged public var status: String?
    @NSManaged public var detailsLink: String?
    @NSManaged public var connectedBusinesses: NSSet?

}

// MARK: Generated accessors for connectedBusinesses
extension Job {

    @objc(addConnectedBusinessesObject:)
    @NSManaged public func addToConnectedBusinesses(_ value: Business)

    @objc(removeConnectedBusinessesObject:)
    @NSManaged public func removeFromConnectedBusinesses(_ value: Business)

    @objc(addConnectedBusinesses:)
    @NSManaged public func addToConnectedBusinesses(_ values: NSSet)

    @objc(removeConnectedBusinesses:)
    @NSManaged public func removeFromConnectedBusinesses(_ values: NSSet)

}
