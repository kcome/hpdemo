//
//  Business+CoreDataProperties.swift
//  HPDemo
//
//  Created by harry on 6/2/20.
//  Copyright © 2020 Harry. All rights reserved.
//
//

import Foundation
import CoreData


extension Business {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Business> {
        return NSFetchRequest<Business>(entityName: "Business")
    }

    @NSManaged public var businessId: Int64
    @NSManaged public var thumbnail: String?
    @NSManaged public var isHired: Bool
    @NSManaged public var parent: Job?

}
